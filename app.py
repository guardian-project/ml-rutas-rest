import json
from flask import Flask, request
from routeBuilder import route_builder

app = Flask(__name__)


@app.route('/')
def hello():
    """ Main page of the app. """
    return "Hello World!"


@app.route('/getRoute', methods = ['GET', 'POST'])
def predict():
    """ Return JSON serializable output from the model """
    payload = request.get_json()
    ini = payload["ini"]
    start  = payload['start']
    end    = payload['end']
    start_lat_lng = (start['lat'], start['long'])
    end_lat_lng   = (end['lat'], end['long'])

    route = route_builder(start_lat_lng,
                          end_lat_lng)
    return({'data': route})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
