import osmnx as ox
import networkx as nx


def make_route(G, start, end, optimizer = 'length'):
  
    G2 = G.copy()
    solved = False

    while not solved : 
        try : 
            orig_node = ox.nearest_nodes(G2, start[1], start[0])
            dest_node = ox.nearest_nodes(G2, end[1], end[0])
            shortest_route = nx.shortest_path(G2, orig_node, dest_node, weight=optimizer)
            solved = True        
        except nx.exception.NetworkXNoPath:
            G2.remove_nodes_from([orig_node, dest_node])
            
    return(shortest_route)

def route_builder(start_latlng, end_latlng):
    graph = ox.load_graphml('./graphs/Spain.graphml') 
    shortest_route = make_route(graph, start_latlng, end_latlng)
    route = [(graph.nodes[node_id]['x'], graph.nodes[node_id]['y']) for node_id in shortest_route]
   
    return(route)
